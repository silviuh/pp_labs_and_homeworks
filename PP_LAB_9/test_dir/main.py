import collections
import os
import struct

from FileTypes import *


class FilesFactoryProvider:

    def __init__(self, file_path: str, frequency_map: list, file_content: bytes):
        self.file_freq_list = frequency_map
        self.file_content = file_content
        self.absolute_file_path = file_path

    def new_instance(self):
        freq_median: float
        max_freq: int = 0
        key_for_max_freq: int = 0
        nr_of_bytes: int = 0
        fault_limit: float = 0
        positive_test_for_uniform_distribution: int = 0

        for key, val in bytes_map.items():
            if val > max_freq:
                max_freq = val
                key_for_max_freq = key
            nr_of_bytes += val

        freq_median = nr_of_bytes / 255
        fault_limit = freq_median / 2

        if 0 in bytes_map.keys():
            if (bytes_map[0] / float(nr_of_bytes)) * 100 >= 30.00:
                return Binary(self.absolute_file_path,
                              self.file_freq_list,
                              os.path.basename(self.absolute_file_path))  # BINARY

        for val in bytes_map.values():
            if abs(val - freq_median) <= fault_limit:
                positive_test_for_uniform_distribution += 1

        # if positive_test_for_uniform_distribution >= 255 / float(2): # does not work all the time
        if self.file_content.startswith(b'BM'):
            header_size = struct.unpack("<HH", self.file_content[14:18])[0]

            if header_size == 12:
                w, h = struct.unpack("<HH", self.file_content[18:22])
                width = int(w)
                height = int(h)
            elif header_size >= 40:
                w, h = struct.unpack("<ii", self.file_content[18:26])
                width = int(w)
                # as h is negative when stored upside down
                height = abs(int(h))
                bpp = len(self.file_content) * 8 / float(width) / float(height)

            return BMP(self.absolute_file_path,
                       self.file_freq_list,
                       width,
                       height,
                       bpp,
                       os.path.basename(self.absolute_file_path))  # BMP

        test_for_xml_file = str(self.file_content[0:100], encoding="UTF-8")
        if "<?xml" in test_for_xml_file:  # i know, i know, it require a better validation method
            return XMLFile(self.absolute_file_path,
                           self.file_freq_list,
                           test_for_xml_file,
                           os.path.basename(self.absolute_file_path))  # XML

        return ASCIIText(self.absolute_file_path,
                         self.file_freq_list,
                         os.path.basename(self.absolute_file_path))  # UTF-8 OR UTF-16

        #  if (re.search(r'<\w{1}xml', test_for_xml_file


class FileReader:
    chunk_size = 8192

    def __init__(self, file_path: str):
        self.parse_path = file_path
        self.file_content = b""

    def parse_file(self):
        with open(self.parse_path, "rb") as file:
            while True:
                chunk = file.read(self.chunk_size)
                self.file_content += chunk

                if chunk:
                    for curr_byte in chunk:
                        if curr_byte not in bytes_map:
                            bytes_map[curr_byte] = 0
                        else:
                            bytes_map[curr_byte] += 1
                else:
                    break

    def get_file_content(self):
        return self.file_content


if __name__ == '__main__':

    ROOT_DIR = os.path.dirname(os.path.abspath("/home/silviuh/PycharmProjects/PP_HOMEWORK_6"))
    for root, subdirs, files in os.walk(ROOT_DIR):
        for file in os.listdir(root):
            file_path = os.path.join(root, file)

            if os.path.isfile(file_path):
                bytes_map = collections.OrderedDict({})
                my_file_reader = FileReader(file_path)
                my_file_reader.parse_file()
                factory = FilesFactoryProvider(file_path, bytes_map, my_file_reader.get_file_content())
                print(factory.new_instance())
                print("\n")



    #bytes_map = collections.OrderedDict({})
    #my_file_reader = FileReader("index_nou.bmp")
    #my_file_reader.parse_file()

    #factory = FilesFactoryProvider("index_nou.bmp", bytes_map, my_file_reader.get_file_content())
    #print(factory.new_instance())
