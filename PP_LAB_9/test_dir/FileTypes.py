class GenericFile:

    def get_path(self):
        raise NotImplementedError("get_path is not implemented...")

    def get_freq(self):
        raise NotImplementedError("get_path is not implemented...")

    def __repr__(self):
        raise NotImplementedError("repr is not implemented")


class ASCIIText(GenericFile):

    absolute_path: str
    frequency: dict
    name: str

    def __init__(self, absolute_file_path: str, freq_dict, name: str):
        self.absolute_path = absolute_file_path
        self.frequency = freq_dict
        self.name = name

    def get_freq(self):
        return self.frequency

    def get_path(self):
        return self.absolute_path

    def __repr__(self):
        return '%s --> %s' % (self.name, "ASCIIText")



class UNICODEText(GenericFile):

    absolute_path: str
    frequency: list
    name: str

    def __init__(self, absolute_file_path: str, freq_dict, name: str):
        self.absolute_path = absolute_file_path
        self.frequency = freq_dict
        self.name = name

    def get_freq(self):
        return self.frequency

    def get_path(self):
        return self.absolute_path

    def __repr__(self):
        return '%s --> %s' % (self.name, "UNCODEText")



class Binary(GenericFile):

    absolute_path: str
    frequency: list
    name: str

    def __init__(self, absolute_file_path: str, freq_dict, name: str):
        self.absolute_path = absolute_file_path
        self.frequency = freq_dict
        self.name = name

    def get_freq(self):
        return self.frequency

    def get_path(self):
        return self.absolute_path

    def __repr__(self):
        return '%s --> %s' % (self.name, "Binary")



class XMLFile(ASCIIText):
    first_tag: str

    def __init__(self, absolute_file_path: str, freq_dict, first_tag, name: str):
        super().__init__(absolute_file_path, freq_dict, name)
        self.first_tag = first_tag

    def get_first_tag(self):
        return self.first_tag

    def __repr__(self):
        return '%s --> %s' % (self.name, "XML")


class BMP(Binary):
    width: int
    height: int
    bpp: int

    def __init__(self, absolute_file_path: str, freq_dict, width, height, bpp, name: str):
        super().__init__(absolute_file_path, freq_dict, name)
        self.width = width
        self.height = height
        self.bpp = bpp

    def show_info(self):
        return self.width, self.height, self.bpp

    def __repr__(self):
        return '%s --> %s' % (self.name, "BMP")
