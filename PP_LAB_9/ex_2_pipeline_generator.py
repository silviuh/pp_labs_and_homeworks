import os


def file_filter(paths):
    for file_path in paths:
        if os.path.isfile(file_path):
            yield file_path


def text_files_filter(paths):
    for file_path in paths:
        if file_path.endswith(".txt"):
            yield file_path


def count_lines(paths):
    for file_path in paths:
        if file_path is not None:
            yield (os.path.basename(file_path),
                   sum(1 for line in open(file_path)))


def convert_to_printable_format(data_tuples):
    for data_pair in data_tuples:
        (yield 'FILE [ {} ] --> {}'.format(data_pair[0], data_pair[1]))


if __name__ == '__main__':
    file_list = [
        "/home/silviuh/PycharmProjects/PP_LAB_9/test_dir/FileTypes.py",
        "/home/silviuh/PycharmProjects/PP_LAB_9/test_dir/html_converter.ui",
        "/home/silviuh/PycharmProjects/PP_LAB_9/test_dir/index_nou.bmp",
        "/home/silviuh/PycharmProjects/PP_LAB_9/test_dir/input_file.txt",
        "/home/silviuh/PycharmProjects/PP_LAB_9/test_dir/main.py",
        "/home/silviuh/PycharmProjects/PP_LAB_9/test_dir/new_utf8.txt",
        "/home/silviuh/PycharmProjects/PP_LAB_9/test_dir/receiver.o",
        "/home/silviuh/PycharmProjects/PP_LAB_9/test_dir/utf-16.txt",
    ]

    pipelined_files = convert_to_printable_format(
        count_lines(
            text_files_filter(
                file_filter(
                    file_list)
            )
        )
    )

    for data_print_format in pipelined_files:
        print(data_print_format)
