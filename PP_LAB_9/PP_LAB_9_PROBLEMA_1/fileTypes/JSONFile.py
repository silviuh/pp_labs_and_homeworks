from fileTypes.File import File


class JSONFile(File):
    def __init__(self):
        super().__init__()
        super().read_from_stdin()

    def print_json(self):
        json_file = open("json_file_test.json", "a")

        json_file.write("JsonObj = {\n");
        json_file.write("\t\"authorName\":{},\n".format(self.author))
        json_file.write("\t\"title\":{},\n".format(self.title))

        json_file.write("\t\"paragraphs\": {\n")
        for i in range(len(self.paragraphs)):
            json_file.write("\t\t\"p{}\":{},\n".format(i, self.paragraphs[i]))
        json_file.write("\t}")
        json_file.write("}")

        json_file.close()