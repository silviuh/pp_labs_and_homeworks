import sys
from abc import ABCMeta


class File:
    __metaclass__ = ABCMeta
    title: str = ""
    author: str = ""
    paragraphs: list = []

    def read_from_stdin(self):
        self.title = str(input("[ TITLE ]: "))
        self.author = str(input("[ AUTHOR ]: "))
        number_of_para = int(input("[ NR_OF_PARA ]: "))

        for i in range(number_of_para):
            print("\t__PARA:")
            input_text = "".join(sys.stdin.readlines())
            self.paragraphs.append(input_text)
