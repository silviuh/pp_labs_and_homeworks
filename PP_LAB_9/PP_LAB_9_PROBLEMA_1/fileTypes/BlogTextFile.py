from fileTypes.TextFile import TextFile


class BlogTextFile(TextFile):
    def __init__(self):
        super().__init__()
        self.read_from_stdin()
        self.template = "Blog"

    def print_text(self):
        blog_text_file = open("blogFile.txt", "a")

        blog_text_file.write(self.title + "\n")
        for _ in self.paragraphs:
            blog_text_file.write(_ + "\n")

        blog_text_file.write("\n\nWritten by {}".format(self.author))

        blog_text_file.close()


