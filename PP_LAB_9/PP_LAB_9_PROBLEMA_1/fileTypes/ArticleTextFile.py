from fileTypes.TextFile import TextFile


class ArticleTextFile(TextFile):
    def __init__(self):
        super().__init__()
        super().read_from_stdin()
        self.template = "Article"

    def print_text(self):
        article_text_file = open("article_text_file.txt", "a")

        article_text_file.write("\t\t\t\t" + self.title + "\n")
        article_text_file.write("\t\t\t\t\tby {}\n".format(self.author))
        for _ in self.paragraphs:
            article_text_file.write(_ + "\n")

        article_text_file.close()