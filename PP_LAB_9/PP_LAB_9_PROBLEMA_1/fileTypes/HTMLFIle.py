import sys

from fileTypes.File import File


class HTMLFile(File):
    def __init__(self):
        super().__init__()
        self.read_from_stdin()

    def print_html(self):
        html_file = open("html_file.html", "a")
        html_file.write("<html>")

        html_file.write("\n\t<head>")
        html_file.write("\n\t\t<meta name={}>".format(self.author))
        html_file.write("\n\t</head>")

        html_file.write("\n\t<title>{}</title>".format(self.title))
        html_file.write("\n\t<body>")

        for para in self.paragraphs:
            html_file.write("\n\t<p>{}\t</p>".format(para))

        html_file.write("\n<html>")

        html_file.close()

    def read_from_stdin(self):
        super(HTMLFile, self).read_from_stdin()