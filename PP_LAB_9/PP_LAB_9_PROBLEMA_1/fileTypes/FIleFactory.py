from fileTypes.ArticleTextFile import ArticleTextFile
from fileTypes.BlogTextFile import BlogTextFile
from fileTypes.File import File
from fileTypes.HTMLFIle import HTMLFile
from fileTypes.JSONFile import JSONFile


class FileFactory:
    HTML_FILE = "HTML_FILE"
    JSON_FILE = "JSON_FILE"
    ARTICLE_TEXT_FILE = "ARTICLE_TEXT_FILE"
    BLOG_TEXT_FILE = "BLOG_TEXT_FILE"

    @staticmethod
    def create_file(file_type: str) -> File:
        switcher = {
            "HTML_FILE": HTMLFile(),
            "JSON_FILE": JSONFile(),
            "ARTICLE_TEXT_FILE": ArticleTextFile().clone(),
            "BLOG_TEXT_FILE": BlogTextFile().clone(),
        }

        return switcher.get(file_type)
