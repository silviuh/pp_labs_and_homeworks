import copy
from abc import ABCMeta, abstractmethod
from distutils.text_file import TextFile

from fileTypes.File import File


class TextFile(File):
    __metaclass__ = ABCMeta
    template: str = "TextFile"

    def __init__(self):
        super().__init__()

    @abstractmethod
    def print_text(self):
        text_file = open("text_file.txt", "a")

        text_file.write(self.template + "\n")
        text_file.write("Titlu: " + self.title + "\n")
        text_file.write("Autor: " + self.author + "\n")
        text_file.write("Continut: \n")

        for _ in self.paragraphs:
            text_file.write(_ + "\n")

        text_file.write("\n\nWritten by {}".format(self.author))

        text_file.close()

    def clone(self) -> TextFile:
        return copy.copy(self)