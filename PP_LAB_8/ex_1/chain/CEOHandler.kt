package chain

class CEOHandler(var rightHandler: Handler? = null, var downHandler: Handler? = null, var upHandler: Handler? = null, var leftHandler: Handler? = null) : Handler {

    @Override
    override fun handleRequest(forwardDirection: String, messageToBeProcessed: String) {
        val data = messageToBeProcessed.split(":")

        when (data[0]) {
            "0" -> println("CEOHandler : handlingRequest...")
            "1", "2", "3" -> when (forwardDirection) {
                "right" -> rightHandler!!.handleRequest("right", messageToBeProcessed)
                "down" -> {
                    if (downHandler != null)
                        downHandler!!.handleRequest("down", messageToBeProcessed)
                }
                "up" -> {
                    if (upHandler != null)
                        upHandler!!.handleRequest("up", messageToBeProcessed)
                }
            }
        }
    }

    @Override
    override fun setDirections(rightHandler: Handler?, downHandler: Handler?, upHandler: Handler?, leftHandler: Handler?) {
        this.rightHandler = rightHandler
        this.downHandler = downHandler
        this.upHandler = upHandler
        this.leftHandler = leftHandler
    }

}