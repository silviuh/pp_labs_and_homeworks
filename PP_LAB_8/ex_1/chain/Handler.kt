package chain

open interface Handler {
    fun handleRequest(forwardDirection: String, messageToBeProcessed:String)
    fun setDirections(rightHandler: Handler?, downHandler: Handler?, upHandler: Handler?, leftHandler: Handler?)
}