package chain

class ManagerHandler(var rightHandler: Handler? = null, var downHandler: Handler? = null, var upHandler: Handler? = null, var leftHandler: Handler? = null) : Handler {

    override fun handleRequest(forwardDirection: String, messageToBeProcessed: String) {
        val data = messageToBeProcessed.split(":")

        when (data[0]) {
            "2" -> println("ManagerHandler: handlingRequest...")
            "3" -> when (forwardDirection) {
                "right" -> rightHandler!!.handleRequest("right", messageToBeProcessed) // in dreapta ma pot duce mereu
                "left" -> leftHandler!!.handleRequest("left", messageToBeProcessed) // in dreapta ma pot duce mereu
                "down" -> {
                    if (downHandler != null)
                        downHandler!!.handleRequest("down", messageToBeProcessed)
                }  // verific daca sunt pe chain ul 2
                "up" -> {
                    if (upHandler != null) {
                        downHandler!!.handleRequest("down", messageToBeProcessed)
                    }  // in caz ca sunt pe chain ul 2, are sens sa ma duc up
                }
            }
        }
    }

    @Override
    override fun setDirections(rightHandler: Handler?, downHandler: Handler?, upHandler: Handler?, leftHandler: Handler?) {
        this.rightHandler = rightHandler
        this.downHandler = downHandler
        this.upHandler = upHandler
        this.leftHandler = leftHandler
    }
}