import chain.CEOHandler
import chain.HappyWorkerHandler
import chain.ManagerHandler
import factory.EliteFactory
import factory.FactoryProducer
import factory.HappyWorkerFactory

fun main() {

    val factoryProducer = FactoryProducer()
    val eliteFactory = factoryProducer.getFactory("EliteFactory")
    val happyWorkerFactory = factoryProducer.getFactory("happyFactory")

    var ceoHandler_chain_1 = eliteFactory.getHandler("CEOHandler")
    var ceoHandler_chain_2 = eliteFactory.getHandler("CEOHandler")

    var executiveHandler_chain_1 = eliteFactory.getHandler("ExecutiveHandler")
    var executiveHandler_chain_2 = eliteFactory.getHandler("ExecutiveHandler")

    var ManagerHandler_chain_1 = eliteFactory.getHandler("ManagerHandler")
    var ManagerHandler_chain_2 = eliteFactory.getHandler("ManagerHandler")

    var happyWorkerHandler_chain_1 = happyWorkerFactory.getHandler("")
    var happyWorkerHandler_chain_2 = happyWorkerFactory.getHandler("")

    ceoHandler_chain_1.setDirections(
            executiveHandler_chain_1,
            ceoHandler_chain_2,
            null,
            null)

    ceoHandler_chain_2.setDirections(
            executiveHandler_chain_2,
            null,
            ceoHandler_chain_1,
            null)

    executiveHandler_chain_1.setDirections(
            ManagerHandler_chain_1,
            executiveHandler_chain_2,
            null,
            ceoHandler_chain_1)

    executiveHandler_chain_2.setDirections(
            ManagerHandler_chain_2,
            null,
            executiveHandler_chain_1,
            ceoHandler_chain_2)

    ManagerHandler_chain_1.setDirections(
            happyWorkerHandler_chain_1,
            ManagerHandler_chain_2,
            null,
            executiveHandler_chain_1)

    ManagerHandler_chain_2.setDirections(
            happyWorkerHandler_chain_2,
            null,
            ManagerHandler_chain_1,
            executiveHandler_chain_2)

    happyWorkerHandler_chain_1.setDirections(
            null,
            happyWorkerHandler_chain_2,
            null,
            ManagerHandler_chain_1)

    ManagerHandler_chain_2.setDirections(
            null,
            null,
            happyWorkerHandler_chain_1,
            ManagerHandler_chain_2)


    ceoHandler_chain_1.handleRequest("right", "2:CEVA")
    happyWorkerHandler_chain_1.handleRequest("down", "3:ALTCEVA")
    ManagerHandler_chain_1.handleRequest("right", "3:BANANA")

}