package factory

import chain.*

class EliteFactory :AbstractFactory() {

    override fun getHandler(handler: String): Handler {
       when (handler){
           "CEOHandler" -> return CEOHandler()
           "ExecutiveHandler" -> return ExecutiveHandler()
           "ManagerHandler" -> return ManagerHandler()
           else -> return UndefinedHandler()
       }
    }
}