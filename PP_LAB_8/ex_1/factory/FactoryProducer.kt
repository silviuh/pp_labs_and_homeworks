package factory

class FactoryProducer {
    fun getFactory(choice: String): AbstractFactory {
        when (choice) {
            "EliteFactory" -> return EliteFactory()
            else -> return HappyWorkerFactory()
        }
    }
}