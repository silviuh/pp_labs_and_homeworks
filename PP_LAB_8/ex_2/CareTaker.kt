class CareTaker {
    var savedStates: ArrayList<SnapShot>? = null

    init{
        savedStates = ArrayList()
    }

    fun addMemento(memento: SnapShot) {
        savedStates!!.add(memento)
    }

    fun getSavedSnapShots() : ArrayList<SnapShot>? {
        return this.savedStates
    }
}