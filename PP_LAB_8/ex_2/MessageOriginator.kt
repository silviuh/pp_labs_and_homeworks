open class MessageOriginator(var careTakerReceived: CareTaker) : MessageFlux() {
    var numberOfSmallMessagesReceived: Int = 0
    var numberOfBigMessagesReceived: Int = 0
    var careTaker: CareTaker? = null

    init {
        this.careTaker = careTakerReceived
    }

    fun setMessageState(Message: String) {
        if (Message.length <= 7)
            this.numberOfSmallMessagesReceived++
        else
            this.numberOfBigMessagesReceived++


        if (this.numberOfBigMessagesReceived % 7 == 0 && this.numberOfBigMessagesReceived != 0) {
            this.restoreFromMemento(
                careTaker!!
                    .getSavedSnapShots()!![careTaker!!
                    .getSavedSnapShots()!!
                    .size % 7]
            )

            this.Message += " ---> Special Event: once 10 big Messages"
            this.numberOfBigMessagesReceived = 0

        } else if (this.numberOfSmallMessagesReceived % 10 == 0 && this.numberOfSmallMessagesReceived != 0) {
            this.restoreFromMemento(
                careTaker!!
                    .getSavedSnapShots()!![careTaker!!
                    .getSavedSnapShots()!!
                    .size % 10]
            )

            this.Message += " ---> Special Event: once 7 small Messages"
            this.numberOfSmallMessagesReceived = 0

        } else {
            this.Message = Message
        }

        careTaker!!.addMemento(createSnapshot())
        this.notifyAllSubscribers()
    }

    fun createSnapshot(): SnapShot {
        return SnapShot(Message)
    }

    fun restoreFromMemento(memento: SnapShot) {
        this.Message = memento.getCurrentState()
    }


}