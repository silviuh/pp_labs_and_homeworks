class SmallWordSubscriber : ISubscriber {
    var buffer: String = ""

    override fun update(Message: String) {
        if (Message.length <= 7) {
            println("[ SmallWordSubscriber ] : received --> $Message")
            buffer = Message
        }
    }
}