open class MessageFlux() {
    var subscribers: ArrayList<ISubscriber>? = null
    var Message: String = ""

    init {
        subscribers = ArrayList()
    }

    fun add(subscriber: ISubscriber) {
        subscribers!!.add(subscriber)
    }

    fun remove(subscriber: ISubscriber) {
        subscribers!!.remove(subscriber)
    }

    fun notifyAllSubscribers() {
        for( i in  0 until subscribers!!.size){
            subscribers!![i].update(this.Message)
        }
    }

}