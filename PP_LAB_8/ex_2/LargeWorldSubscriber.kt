class LargeWorldSubscriber :ISubscriber {
    var buffer :String = ""

    override fun update(Message: String) {
        if(Message.length > 7){
            println("[ LargeWorldSubscriber ] : received --> $Message")
            buffer = Message
        }
    }
}