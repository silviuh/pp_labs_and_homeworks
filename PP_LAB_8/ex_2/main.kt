import java.io.File

fun main() {

    var fileContent = File("/home/silviuh/IdeaProjects/PP_LAB_8_PROB_2/src/lorem_ipsum.txt").inputStream().readBytes().toString(Charsets.UTF_8)
    var words = fileContent.split("[\\s]".toRegex())

    var smallWordSubscriber: ISubscriber = SmallWordSubscriber()
    var bigWordSubscriber: ISubscriber = LargeWorldSubscriber()
    var mementoManager: CareTaker = CareTaker()
    var originator: MessageOriginator = MessageOriginator(mementoManager)

    originator.add(smallWordSubscriber)
    originator.add(bigWordSubscriber)

    for (word in words) {
        originator.setMessageState(word)
    }


}