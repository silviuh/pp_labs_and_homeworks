fun String.toPascalCase0(): String {
    val components = this.split(" ")
    var result: String = ""
    for (component in components) {
        result += component.capitalize()
    }
    return result
}

class TransformFunctor<K, V>(val input: MutableMap<K, V>) {
    fun map(function: (V) -> V): TransformFunctor<K, V> {
        val result = mutableMapOf<K, V>()
        for (item in input) {
            result.put(item.key, function(item.value))
        }

        return TransformFunctor(result)
    }
}

fun main() {
    val myMap = mutableMapOf<Int, String>()
    myMap.put(1, "sarmala aaa bb")
    myMap.put(2, "gorgonzola ccc bb")
    myMap.put(3, "salata kkk")
    myMap.put(4, "branzaa uuu bb")

    print(TransformFunctor(myMap).map { value: String ->
        ("Test " + value)
    }.map {
        it.toPascalCase0()
    }.input)

}