import kotlin.math.sqrt

fun main() {
    var xCoordinates = 1.until(5).toList().shuffled()
    var yCoordinates = 3.until(7).toList().shuffled()
    // var lastResult =
    val list = xCoordinates zip yCoordinates
    val lastPair = list.get(list.size - 1)
    val firstPair = list.get(0)

    val distancesList = (list.zipWithNext { pair1, pair2 ->
        sqrt(
            ((pair1.first - pair2.first) * (pair1.first - pair2.first) +
                    (pair1.second - pair2.second) * (pair1.second - pair2.second)).toDouble()
        )
    }.toMutableList() + (
            ((lastPair.first - firstPair.first) * (lastPair.first - firstPair.first) +
                    (lastPair.second - firstPair.second) * (lastPair.second - firstPair.second)).toDouble()
            )).sum()

    print(distancesList)

}