fun main() {

    val myList = mutableListOf<Int>(1, 21, 75, 39, 7, 2, 35, 3, 31, 7, 8)
    val result = myList.asSequence()
        .filter { it > 5 }.zipWithNext { a, b ->
            a * b
        }
        .filterIndexed { index, value -> index % 2 == 0 }
        .toMutableList()
        .sum()

    print(result)

}

