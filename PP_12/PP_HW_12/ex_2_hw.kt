import java.io.File

fun readFileAsTextUsingInputStream(fileName: String) = File(fileName).inputStream().readBytes().toString(Charsets.UTF_8)

fun String.caesarEncrypt(offset: Int): String {
    return this.asSequence().map { c: Char ->
        ((c - 'a' + offset) % 26).toChar() + 'a'.toInt()
    }.joinToString("")
}

fun main() {
    val result = readFileAsTextUsingInputStream("/home/silviuh/IdeaProjects/PP_LAB_12/src/ex_file")
        .split(" ")
        .toList().asSequence().filter {
            it.length in 4..7
        }
        .toList()
        .map {
            it.caesarEncrypt(3)
        }

    print(result)
}