import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.EnumSet.range
import kotlin.properties.Delegates


// PROBLEMA 1
fun Int.isPrime(): Boolean {
    if (this < 2)
        return false
    if (this == 2)
        return true
    var i = 2

    while (i * i <= this) {
        if (this % i == 0)
            return false
        i++
    }
    return true
}

// PROBLEMA 2
fun String.convertToDate(formatter: DateTimeFormatter): LocalDate? {
    return LocalDate.parse(this, formatter)
}

// PROBLEMA 4
var myIntPrime: Int by Delegates.vetoable(2) { property, oldValue, newValue ->
    println("${property.name}: ($oldValue -> $newValue)")
    newValue.isPrime()
}

fun main() {

    /*
    myIntPrime = 4
    // PROBLEMA 3
    val exMap = mutableMapOf<String, Int>()
    exMap.put("sal", 1)
    exMap.put("salsa", 2)
    exMap.put("catel", 3)
    exMap.put("branzqa", 4)
    println(exMap.map {
        mapOf(it.value to it.key)
    })

    print(37.isPrime())
    println("2017-07-25".convertToDate(DateTimeFormatter.ISO_DATE))
    */


    // PROBLEMA 5
    /*
    val nrOfTimes = readLine()?.toInt()
    val result = 1.until(5).toList().asSequence().map { i: Int ->
        MutableList(nrOfTimes!!) { i }
    }.toList().flatMap { it }

    print(result)
     */


    // PROBLEMA 6
    //val myString = readLine()?.toString()
    //println(
    //   myString!!.asSequence().distinct().joinToString(separator = "")
    //)

    // PROBLEMA 7

    val myString = readLine()?.toString()
    println(
        myString!!.asSequence()
            .map {
                val result = myString.count { c: Char -> c == it }

                if (result == 1)
                    "$it"
                else
                    "$it$result"
            }.distinct().joinToString("")
    )

//filter {
//            (myString.count { c: Char -> c == it }) > 1
}

