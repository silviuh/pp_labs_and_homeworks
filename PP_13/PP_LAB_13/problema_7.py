from functools import partial
from itertools import starmap


def wrapee(args, k):
    return list(map(lambda element: element * element + k, args))  # aici putem converti in ce dorim list, seq, etc


def prob_7_starmap(iterable, k):
    return list(starmap(lambda *args: tuple([element * element + k for element in args]), iterable))


if __name__ == '__main__':
    ptr_8 = partial(wrapee, k=8)
    print(ptr_8([8, 9, 6, 4]))
    print(prob_7_starmap([(8, 7), (9, 1), (6, 4), (4, 3)], 3))
