from math import ceil, sqrt
from functional import seq
from collections import namedtuple


class MyString(str):
    pass


# PROBLEMA 3
def zip(*iterables):
    return map(lambda *args: tuple(args), *iterables)
    # return map(lambda *items: (*items,), *iterables)


# PROBLEMA 2
def to_pascal_case(input: str):
    result = ""
    tokens = input.split(" ")
    for token in tokens:
        result += token.title()

    return result

# PROBLEMA 5
def eliminate_duplicates(input: str):
    return "".join(
        seq(list(input)).distinct()
    )


# PROBLEMA 6
def problema6(sir: str):
    result2 = list(
        seq(list(sir)).distinct().map(lambda c: (c, sir.count(c))).reduce_by_key(lambda x, y: x + y))
    result2.sort()
    return "".join(["".join(pairo[0] + str(pairo[1]))
                    for pairo in result2 if pairo[1] != 1])

    # map(lambda pairo: (pairo[0] if pairo[1] == 1 else (pairo[0] + str(pairo[1]))))
    # return str([k + v for k, v in
    #           seq(zip(list(aux), [aux.count(c) for c in aux])).map(lambda pair: pair[0] if pair[1] == 1 else pair)])
    # return zip(list(aux), [aux.count(c) for c in aux])
    # result = (map_reduce('abbbccc', key_func, valufunc, reducefunc))


if __name__ == '__main__':
    list0 = list(range(9))
    list1 = list('abcdefgh')
    list2 = list('kp' * 7)

    for item in zip(list0, list1, list2):
        print(item)

    print(problema6("aaabbbcccd"))


    MyString.pascal_case = to_pascal_case
    string = MyString("salata beouf")
    print(string.pascal_case())
    print(eliminate_duplicates("aaaabbbbccd"))


    # PROBLEMA 4
    n = int(input("ZI FRATE N UL: "))
    result = filter(
        lambda x: x % 2 == 0,
        (n * n for n in range(ceil(sqrt(n))))
    )
    for square in result:
        print(square)
