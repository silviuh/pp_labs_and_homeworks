import operator
from functools import reduce
from itertools import starmap, product

if __name__ == '__main__':
    my_list = [1, 21, 75, 39, 7, 2, 35, 3, 31, 7, 8]
    modified_list = list(filter(lambda x: x > 5, my_list))
    result = list(zip(modified_list[0::2], modified_list[1::2]))
    final_result = sum(list(starmap(lambda *pair: reduce(operator.mul, pair, 1), result)))
    print(final_result)
