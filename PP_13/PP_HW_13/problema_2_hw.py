if __name__ == '__main__':
    data = 'This sentence has words of various lengths in it, both short ones and long ones'.split()
    key_func = lambda word: word[0]
    print(more_itertools.map_reduce(data, key_func)
