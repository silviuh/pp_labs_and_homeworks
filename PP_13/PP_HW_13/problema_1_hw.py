from datetime import datetime

from functional import seq

SECONDS_IN_A_YEAR = 31556952


class Person:
    def __init__(self, first_name: str, last_name: str, date_of_birth, email_adress):
        self.email_adress = email_adress
        self.first_name = first_name
        self.date_of_Birth = date_of_birth
        self.last_name = last_name

    def __str__(self):
        return f'{self.first_name}, {self.last_name}, {self.date_of_Birth}, {self.email_adress}'


if __name__ == '__main__':
    persons = [
        Person("John", "Doe", datetime(1960, 11, 3), "jdoe@example.com"),
        Person("Ellen", "Smith", datetime(1992, 5, 13), "ellensmith@example.com"),
        Person("Jane", "White", datetime(1986, 2, 1), "janewhite@example.com"),
        Person("Bill", "Jackson", datetime(1999, 11, 6), "jdoe@example.com"),
        Person("John", "Smith", datetime(1975, 7, 14), "bjackson@example.com"),
        Person("Jack", "Williams", datetime(2005, 5, 28), "johnsmith@example.com"),
        Person("Sharam", "Williams", datetime(2005, 5, 28), "")
    ]

    #  sort by age and find youngest and oldest person
    sorted_seq_by_age = (seq(persons).sorted(key=lambda person: person.date_of_Birth))
    for person in sorted_seq_by_age:
        print(person)
    print(sorted_seq_by_age.first())
    print(sorted_seq_by_age.last())

    # filter underage
    print("------------------------------------------------------------------")
    filtered_seq_by_underage = (
        seq(persons).filter(
            lambda person: (datetime.now() - person.date_of_Birth).total_seconds() / SECONDS_IN_A_YEAR < 18))
    for person in filtered_seq_by_underage:
        print(person)

    # list of emails
    print("------------------------------------------------------------------")
    emails = list(map(lambda person: person.email_adress, persons))
    print(emails)

    # map of name and email, am facut invers dar se poate schimba doar ordinea
    print("------------------------------------------------------------------")
    names_and_emails = seq(
        zip(
            list(map(lambda person: person.email_adress, persons)),
            list(map(lambda person: person.first_name + " " + person.last_name, persons))
        )
    ).to_dict()
    print(names_and_emails)

    # map of email and person
    print("------------------------------------------------------------------")
    email_person_map = seq(
        zip(
            list(map(lambda person: person.email_adress, persons)),
            persons
        )
    ).to_dict()
    print(email_person_map)

    print("------------------------------------------------------------------")
    # group by month of birthday
    grouped_by_birthday = list(seq(persons).group_by(lambda person: person.date_of_Birth))
    print(grouped_by_birthday)

    print("------------------------------------------------------------------")
    # partition
    partitions = (seq(persons).partition(lambda person: person.date_of_Birth.year <= 1980))
    for group in partitions:
        print(group)

    print("------------------------------------------------------------------")
    # distinct first names
    distinct_first_names = " ".join(
        [person.first_name for person in (seq(persons).distinct_by(lambda person: person.first_name))]
    )
    print(distinct_first_names)

    print("------------------------------------------------------------------")
    # average age
    average_age = seq(persons).average(lambda person: (datetime.now() - person.date_of_Birth).total_seconds())
    print(str(average_age / SECONDS_IN_A_YEAR) + " years")

    print("------------------------------------------------------------------")
    # count
    smiths = seq(persons).count(lambda person: person.last_name == "Smith")
    print(smiths)

    print("------------------------------------------------------------------")
    # find any with optional result
    findings = seq(persons).map(
        lambda person: person.first_name if person.first_name == "John" else "No John was found").filter(
        lambda flag: flag != "No John was found").take(1)
    print(findings)

    print("------------------------------------------------------------------")
    # check with empty email
    empty_emails = (seq(persons).exists(lambda person: person.email_adress == ""))
    print(empty_emails)
