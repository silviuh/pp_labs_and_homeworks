import random
import threading
import time
from queue import Queue
from typing import Optional

random.seed(2)


class ThreadPool:
    container: Queue

    def __init__(self, size):
        self.container = Queue()
        self.threads = []

        for thread_number in range(size):
            self.threads.append(
                CustomThread(self.container,
                             thread_number,
                             True)
            )
        for thread in self.threads:
            thread.start()

    def map(self, task, **kwargs):
        self.container.put(
            (task, kwargs.get("arguments"))
        )

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.destroy_thread_pool()

    def destroy_thread_pool(self):
        self.container.put(
            (self.sentinel, None)
        )
        for thread in self.threads:
            thread.join()

    def sentinel(self):
        pass


class CustomThread(threading.Thread):
    def __init__(self, tasks, thread_id, lives):
        super().__init__()
        self.tasks = tasks
        self.id = thread_id
        self.lives = lives

    def dies(self):
        self.lives = False

    def run(self):
        while self.lives is True:
            task, arguments = self.tasks.get()
            if task.__name__ == 'sentinel':
                self.dies()
                print(f'{self.id}: I died...')
                self.tasks.put(
                    (threadManger.sentinel, None)
                )
            else:
                task(*arguments)
                time.sleep(2)
                self.tasks.task_done()


def task_example(*args):
    sum = 0
    for _ in args:
        sum += _
    print("Caculated sum is: " + str(sum))


if __name__ == '__main__':
    with ThreadPool(4) as threadManger:
        for i in range(20):
            threadManger.map(task_example, arguments=[random.randint(10, 50) for _ in range(10)])
