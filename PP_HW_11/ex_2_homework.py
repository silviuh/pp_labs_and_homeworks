import shlex
import subprocess

if __name__ == '__main__':
    '''
    cmd = "pwd | grep 'silviuh' | wc -l > new_file.txt"
    ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
    print(output)
    '''

    while True:
        command = input().split('|')
        args_for_processes = []
        print(command)

        for sub_command in command:
            args_for_processes.append(shlex.split(sub_command))

        last_process = subprocess.Popen(args_for_processes[0], stdout=subprocess.PIPE)
        first_process = last_process

        for proc_number, arguments in enumerate(args_for_processes):
            if proc_number != 0 and proc_number != len(args_for_processes) - 1:
                proc = subprocess.Popen(arguments, stdin=last_process.stdout, stdout=subprocess.PIPE)
                last_process = proc
            elif proc_number == len(args_for_processes) - 1:
                proc = subprocess.Popen(arguments, stdin=last_process.stdout)
                last_process = proc

        first_process.wait()
        last_process.communicate()[0]
