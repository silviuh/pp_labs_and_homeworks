import asyncio
from queue import Queue
from threading import Thread
import time
import random


async def sum(task_name, number):
    total_sum = 0
    for i in range(1, number):
        await asyncio.sleep(1)
        total_sum += i

    return total_sum
    # print(f"Task {task_name}: sum({total_sum})")


async def main():
    result = await asyncio.gather(
        sum("A", 12),
        sum("B", 14),
        sum("C", 15),
    )

    print(result)


asyncio.run(main())
