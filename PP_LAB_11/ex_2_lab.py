from queue import Queue
from threading import Thread
import time
import random


class ProducerThread(Thread):
    def __init__(self, received_queue):
        super().__init__()
        self.container = received_queue

    def run(self):
        nums = range(5)
        while True:
            num = random.choice(nums)
            self.container.put(num)
            print("Produced", num)
            time.sleep(random.random())


class ConsumerThread(Thread):
    def __init__(self, received_queue):
        super().__init__()
        self.container = received_queue

    def run(self):
        while True:
            num = self.container.get()
            self.container.task_done()
            print("Consumed", num)
            time.sleep(random.random())


if __name__ == '__main__':
    queue = Queue()
    producer = ProducerThread(queue)
    consumer = ConsumerThread(queue)

    producer.start()
    consumer.start()
