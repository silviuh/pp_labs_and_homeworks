import math
import threading
import multiprocessing
from cmath import sqrt
from collections import deque
from concurrent.futures import ThreadPoolExecutor
import time
from random import randint
from multiprocessing import Queue


def is_prime(number):
    for i in range(2, int(sqrt(number)) + 1):
        if number % i == 0:
            return False

    return number > 1


def factorial0(n):
    result = 1;
    for num in range(2, n + 1):
        result *= num
    return result


def factorial1(n):
    return math.factorial(n)


def filter_primes(general_list):
    return filter(is_prime, set(general_list))


global_set = set()
for i in range(1_000_000):
    global_set.add(randint(0, 1_000_000))


def countdown(received_array):
    # received_array.sort()
    # filter_primes(received_array)
    container = received_array.get()
    for element in container:
        element += 1

    received_array.put(container)


def ver_1(received_array):
    # thread_1 = threading.Thread(target=countdown, args=(received_array,))
    # thread_2 = threading.Thread(target=countdown, args=(received_array,))
    thread_1 = threading.Thread(target=countdown, args=(received_array,))
    thread_2 = threading.Thread(target=countdown, args=(received_array,))
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()


def ver_2(received_array):
    countdown(received_array)
    countdown(received_array)


def ver_3(received_array):
    # process_1 = multiprocessing.Process(target=countdown, args=(received_array,))
    # process_2 = multiprocessing.Process(target=countdown, args=(received_array,))
    process_1 = multiprocessing.Process(target=countdown, args=(received_array,))
    process_2 = multiprocessing.Process(target=countdown, args=(received_array,))
    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()


def ver_4(received_array):
    with ThreadPoolExecutor(max_workers=2) as executor:
        future = executor.submit(countdown(received_array))
        future = executor.submit(countdown(received_array))
    # future = executor.submit(countdown(received_array))
    # future = executor.submit(countdown(received_array)


if __name__ == '__main__':
    safe_queue = Queue()
    safe_queue.put([0 for _ in range(1_000_0)])

    # for i in range(0, 1_000):
    #     temp_list = safe_queue.get()
    #     temp_list.append(0)
    #     safe_queue.put(temp_list)
    # ex_arr_1 = []
    # ex_arr_2 = []
    # ex_arr_3 = []
    # ex_arr_4 = []

    # for i in range(1_000_000):
    #     ex_arr_1.append(randint(0, 1_000_000))
    #     ex_arr_2.append(randint(0, 1_000_000))
    #     ex_arr_3.append(randint(0, 1_000_000))
    #     ex_arr_4.append(randint(0, 1_000_000))

    start = time.time()
    ver_1(safe_queue)
    end = time.time()
    print("\n Timp executie pseudoparalelism cu GIL")
    print(end - start)

    start = time.time()
    ver_2(safe_queue)
    end = time.time()
    print("\n Timp executie secvential")
    print(end - start)

    start = time.time()
    ver_3(safe_queue)
    end = time.time()
    print("\n Timp executie paralela cu multiprocessing")
    print(end - start)

    start = time.time()
    ver_4(safe_queue)
    end = time.time()
    print("\n Timp executie paralela cu concurrent.futures")
    print(end - start)

    print(safe_queue)
