import json
import os
from datetime import time, datetime

import recordtype as recordtype
import requests

from GetRequest import GetRequest
from HTTPGenericRequest import HTTPGenericRequest


class CachedRequest(HTTPGenericRequest):
    timeout: float
    get_request: GetRequest
    cache: str
    customized_responses: dict

    def __init__(self, get_request: GetRequest, cache_file: str):
        self.get_request = GetRequest
        self.cache = cache_file
        self.customized_responses = dict()

    def get_response(self) -> str:

        with open(self.cache, "w+") as json_file:
            if os.path.getsize(self.cache) > 0:
                self.customized_responses = json.load(json_file)

                current_time = datetime.now().time().strftime("%H:%M:%S")

                if self.customized_responses[self.get_request.URL] is not None:
                    delta = (datetime.strptime(current_time)
                             - datetime.strptime(self.customized_responses[self.get_request.URL]["timestamp"])).seconds

                    if delta < 3600:  # read from cache
                        return self.customized_response[self.get_request.URL]["response_content"]

                else:
                    request_init_time = datetime.now().time().strftime("%H:%M:%S")
                    response = self.get_request.get_response_base()

                    self.customized_response[self.get_request.URL] = ({
                        'timestamp': request_init_time,
                        'response_content': response.content().decodeToString()
                    })

                    json.dump(self.customized_responses, json_file)
                    return self.customized_response[self.get_request.URL]["response_content"]

            else:
                request_init_time = datetime.now().time().strftime("%H:%M:%S")
                response = self.get_request.get_response_base()

                self.customized_response[self.get_request.URL] = ({
                    'timestamp': request_init_time,
                    'response_content': response.content().decodeToString()
                })

                json.dump(self.customized_responses, json_file)
                return self.customized_response[self.get_request.URL]["response_content"]

        json_file.close()
