package org.example

class Multiply(container: ArrayList<Int>, alpha: Int) : Runnable {
    @Volatile
    var container: ArrayList<Int>? = null
    var constant: Int = 1

    init {
        this.container = container
        this.constant = alpha
    }

    override fun run() {
        for (i in container!!.indices){
            container!![i] = container!![i] * 2
        }
    }
}

class Sort(container: ArrayList<Int>) : Runnable {
    @Volatile
    var container: ArrayList<Int>? = null

    init {
        this.container = container
    }

    override fun run() {
        container!!.sort()
    }
}

class Print(container: ArrayList<Int>) : Runnable {
    @Volatile
    var container: ArrayList<Int>? = null

    init {
        this.container = container
    }

    override fun run() {
        print("Transformed list is: " + container)
    }
}

fun main() {

    var myADT = arrayListOf<Int>(6, 1, 4, 8, 12)
    val multiplyThread = Thread(Multiply(myADT, 4))
    val sortThread = Thread(Sort(myADT))
    val printThread = Thread(Print(myADT))

    multiplyThread.run()
    sortThread.run()
    printThread.run()
}
