package org.example

import kotlinx.coroutines.*
import kotlin.random.Random
import kotlin.system.measureTimeMillis

suspend fun createCoroutines(amount: Int) {
    val jobs = ArrayList<Job>()
    for (i in 1..amount) {
        jobs += GlobalScope.launch {
            var result: Int = 0
            val x = (1..12).shuffled().first()
            // result = (x * (x + 1)) / 2
            for (j in 1..x){
                result += j
            }
            println( "[$i] numarul meu este: " + x.toString() + " si rezultatul este: " + result)
        }
    }

    jobs.joinAll()
}



fun main() = runBlocking{
    val time = measureTimeMillis {
        createCoroutines(4)
    }

    println("procesul a durat: " + time + " [ms]")
}