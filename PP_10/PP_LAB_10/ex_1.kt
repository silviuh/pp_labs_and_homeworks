package com.pp.laborator

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.coroutines.runBlocking
import java.io.File
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.locks.ReentrantLock
import kotlin.system.measureTimeMillis

class ThreadSafeList(filePath: String) {
    private val sharedCounterLock = ReentrantLock()
    private var filePath: String? = null;

    init {
        this.filePath = filePath
    }

    @Volatile
    public var container: MutableList<Int> = mutableListOf()

    fun append(value: Int) {
        try {
            sharedCounterLock.lock()
            container.add(value)
            File(filePath).appendText(value.toString())


        } finally {
            sharedCounterLock.unlock()
        }
    }
}


suspend fun CoroutineScope.massiveRun(action: suspend () -> Unit) {
    val n = 100
    val k = 1000
    val time = measureTimeMillis {
        val jobs = List(n)
        {
            launch { repeat(k) { action() } }
        }
        jobs.forEach { it.join() }
    }
    println("S-au efectuat ${n * k} operatii in $time ms")
}

val mtContext = newFixedThreadPoolContext(2, "mtPool")
var counter = AtomicInteger()


fun main() = runBlocking<Unit> {
    val threadSafeList: ThreadSafeList = ThreadSafeList("exemplu1.txt")

    CoroutineScope(mtContext).massiveRun {
        counter.incrementAndGet() //variabila comuna unde vor aparea erori
        threadSafeList.append(counter.get())

    }

    for (i in threadSafeList.container) {
        print("$i ")
    }
    println("Numarator = $counter")
}
