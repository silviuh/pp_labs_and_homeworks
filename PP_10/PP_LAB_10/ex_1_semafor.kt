package org.example

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.coroutines.runBlocking
import java.io.File
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

fun main(args: Array<String>) = runBlocking {

    /*
    object : Thread() {
        override fun run() {
            println("Sunt in thread-ul singleton ${Thread.currentThread()}")
        }
    }.start()
    val t1 = SimpleThread()
    t1.run()
    val t2 = SimpleRunnable()
    t2.run()
    val thread = Thread {
        println("Thread lambda ${Thread.currentThread()} s-a executat.")
    }
    thread.start()
     */

    var threadId = 0;
    val scope =
        CoroutineScope(newFixedThreadPoolContext(4, "synchronizationPool")) // We want our code to run on 4 threads
    scope.launch {
        val coroutines = 1.rangeTo(1000).map {
            launch {
                Log.Write("BANANA" + threadId + "\n")
                threadId++
            }
        }

        coroutines.forEach { corotuine ->
            corotuine.join() // wait for all coroutines to finish their jobs.
        }
    }.join()

}

object Log {
    val fname = "Semafor.txt"
    var semaphory: Semafor? = Semafor;
    var reentrantLock: ReentrantLock = ReentrantLock()
    private val canWrite = reentrantLock.newCondition()
    var writeable: AtomicBoolean = AtomicBoolean(true)

    fun Write(line: String) {
        /*
        if (semaphory!!.Enter() == true) {
            File(fname).appendText(line)
            semaphory!!.Exit()
        }
         */
        reentrantLock.lock()
        try {
            while (writeable.equals(false)) canWrite.await()
            File(fname).appendText(line)
            writeable.set(false)
            canWrite.signal()
        } finally {
            writeable.set(true)
            reentrantLock.unlock()
        }
    }

    fun Reset() {
        File(fname).delete()
    }
}

object Semafor {
    private val lock = ReentrantLock()
    /*
    @Volatile
    private var lock: Boolean = false
    @Synchronized
    fun Enter(): Boolean {
        if (lock == false) {
            lock = true
            return lock
        }
        return false
    }

    /*
    while (lock == false) {
        try {
            Thread.sleep(1000);
        } catch (exception: InterruptedException){
            exception.printStackTrace()
        }
        finally {
            lock = true
        }
    }

     */
    @Synchronized
    fun Exit() {
        if (lock == true) {
            lock = false
        }
    }
     */
}

