class GateImplementationBridge {

    fun getResult(input: ArrayList<Integer>): AndGate? {
        when (input.size) {  // FSM
            2 -> return TwoInputAnd(input)
            3 -> return ThreeInputAnd(input)
            4 -> return ForInputAnd(input)
            8 -> return EightInputAnd(input)
            else -> return null
        }
    }
}
