class AndDevice {
    val implementation : GateImplementationBridge = GateImplementationBridge()

    fun computeResult(input: ArrayList<Integer>) : Int{
        return implementation.getResult(input)!!.getANDResult()
    }

}