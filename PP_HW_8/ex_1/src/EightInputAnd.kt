class EightInputAnd (input : ArrayList<Integer>) : AndGate{
    open var result : Int = 0

    init{
        result = input[0].toInt()
        for(number in input){
            result = result and number.toInt()
        }
    }

    override fun getANDResult(): Int {
        return result
    }
}