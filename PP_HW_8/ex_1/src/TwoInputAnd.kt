class TwoInputAnd(input : ArrayList<Integer>) : AndGate{
    open var result : Int = 0

    init{
        result = (input[0].toInt() and input[1].toInt())
    }

    override fun getANDResult(): Int {
        return result
    }
}