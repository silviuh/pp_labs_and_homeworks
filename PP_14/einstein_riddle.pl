la_dreapta(X, Y) :- X is Y+1.
la_stanga(X, Y) :- la_dreapta(Y, X).

langa(X, Y) :- la_dreapta(X, Y).
langa(X, Y) :- la_stanga(X, Y).

solution(strada, FishOwner) :-
    strada = [
           casa(1, Nationalitate1,  Culoare1,     Animal1,   Bautura1,    Tigari1),
           casa(2, Nationalitate2,  Culoare2,     Animal2,   Bautura2,    Tigari2),
           casa(3, Nationalitate3,  Culoare3,     Animal3,   Bautura3,    Tigari3),
           casa(4, Nationalitate4,  Culoare4,     Animal4,   Bautura4,    Tigari4),
           casa(5, Nationalitate5,  Culoare5,     Animal5,   Bautura5,    Tigari5)],
    locatar(casa(_, brit,          red,        _,      _,          _           ), strada),
    locatar(casa(_, swede,         _,          dog,    _,          _           ), strada),
    locatar(casa(_, dane,          _,          _,      tea,        _           ), strada),
    locatar(casa(A, _,             green,      _,      _,          _           ), strada),
    locatar(casa(B, _,             white,      _,      _,          _           ), strada),
    la_stanga(A, B),
    locatar(casa(_, _,             green,      _,      coffee,     _           ), strada),
    locatar(casa(_, _,             _,          birds,  _,          pall_mall   ), strada),
    locatar(casa(_, _,             yellow,     _,      _,          dunhill     ), strada),
    locatar(casa(3, _,             _,          _,      milk,       _           ), strada),
    locatar(casa(1, norweigan,     _,          _,      _,          _           ), strada),
    locatar(casa(C, _,             _,          _,      _,          blend       ), strada),
    locatar(casa(D, _,             _,          cats,   _,          _           ), strada),
    langa(C, D),
    locatar(casa(E, _,             _,          horse,  _,          _           ), strada),
    locatar(casa(F, _,             _,          _,      _,          dunhill     ), strada),
    langa(E, F),
    locatar(casa(_, _,             _,          _,      beer, bluemaster        ), strada),
    locatar(casa(_, german,        _,          _,      _,          prince      ), strada),
    locatar(casa(G, norweigan,     _,          _,      _,          _           ), strada),
    locatar(casa(H, _,             blue,       _,      _,          _           ), strada),
    langa(G, H),
    locatar(casa(I, _,             _,          _,      _,          blend       ), strada),
    locatar(casa(J, _,             _,          _,      water,      _           ), strada),
    langa(I, J),
    locatar(casa(_, FishOwner,     _,          fish,   _,          _           ), strada).
